var express = require('express');
var app = express();
var http = require('http');
var fs = require('fs');
var device = require('express-device');
//var compression = require('compression');
//app.use(require('prerender-node').set('prerenderToken', 'GBSxLecvLaZGYojfe202'));
//app.use(compression())
var isProduction = true;
var httpsEnabled = true;

var redis=require('redis');
var cache=null;
var RDS={};
    RDS.HOST="webservicecache.ykhbce.ng.0001.aps1.cache.amazonaws.com",
    RDS.PORT=6379

cache=redis.createClient(RDS.PORT,RDS.HOST).on('connect',function(){
    console.log("REDIS connected");

});



var ENVIRONMENT = "";
app.set('view engine', 'ejs');
app.set('view options', {
	layout: false
});
app.set('views', __dirname + '/');
app.use(device.capture());
device.enableDeviceHelpers(app);
device.enableViewRouting(app, {
	"noPartials": false
});
var post = process.env.PORT || 8080;
//app.use(express.static('production'));
app.use(express.static(ENVIRONMENT + 'assets'));
app.use('/desktop', express.static(ENVIRONMENT + 'desktop'));
app.use('/providers', express.static(ENVIRONMENT + 'providers'));
app.use('/mobile', express.static(ENVIRONMENT + 'phone'));
app.use(express.static(ENVIRONMENT + 'phone/views'));
app.use(express.static(ENVIRONMENT + 'desktop/views'));
app.locals.ismin = (isProduction) ? ".min" : "";
/*app.use(function(req,res,next){
	if (req.headers["x-forwarded-proto"] === "https"){
      next()
    }else{
    res.redirect(301,"https://" + req.headers.host + req.url);
	}
});*/
app.all('/', function (req, res) {
	if (req.device.type == "bot" || (typeof req.get('User-Agent') !== "undefined" && req.get('User-Agent').indexOf('PhantomJS') !== -1)) {

		var data = {};
		data.locale = "";
		got('apiv2.spotboye.com/dashboard', {
			json: true
		}).then(response => {
			data.dashboard = response.body.data;
			res.render('bots-template/dashboard.ejs', data);
		}).catch(error => {
			res.status(404).send("Page not found ");
		});
	} else if (req.device.type == "tablet") {
		res.render('index.ejs', {
			forceType: 'phone'
		});
	} else {
		res.render('index.ejs');
	}
});
app.all("/googlesitemap.xml", function (req, res) {
	res.setHeader("content-type", "application/xml");
	http.get({
		host: 'apiv2.spotboye.com',
		port: 80,
		path: '/googlesitemap.xml'
	}, function (_res) {
		var _data = "";
		_res.on('data', function (chunk) {
			_data += chunk;
		});
		_res.on('end', function () {
			res.send(_data);
		});
	}).on('error', function (e) {
		res.send(e);
	});
});
app.all("/:cat/:sub/sitemap.xml", function (req, res) {
	res.setHeader("content-type", "application/xml");
	http.get({
		host: 'apiv2.spotboye.com',
		port: 80, //80,
		path: '/' + req.params.cat + '/' + req.params.sub + '/sitemap.xml'
	}, function (_res) {
		var _data = "";
		_res.on('data', function (chunk) {
			_data += chunk;
		});
		_res.on('end', function () {
			res.send(_data);
		});
	}).on('error', function (e) {
		res.send(e);
	});
});
app.all("/sitemap.xml", function (req, res) {
	res.setHeader("content-type", "application/xml");
	http.get({
		host: 'apiv2.spotboye.com',
		port: 80, //80,
		path: '/sitemap.xml'
	}, function (_res) {
		var _data = "";
		_res.on('data', function (chunk) {
			_data += chunk;
		});
		_res.on('end', function () {
			res.send(_data);
		});
	}).on('error', function (e) {
		res.send(e);
	});
});
app.all("/generic-sitemap.xml", function (req, res) {
	res.setHeader("content-type", "application/xml");
	http.get({
		host: 'apiv2.spotboye.com',
		port: 80, //80,
		path: '/generic-sitemap.xml'
	}, function (_res) {
		var _data = "";
		_res.on('data', function (chunk) {
			_data += chunk;
		});
		_res.on('end', function () {
			res.send(_data);
		});
	}).on('error', function (e) {
		res.send(e);
	});
});
app.all("/feed/:id/:vendor", function (req, res) {
	res.setHeader("content-type", "application/rss+xml");
	http.get({
		host: 'apiv2.spotboye.com',
		port: 80,
		path: '/feed/' + req.params.id + '/' + req.params.vendor
	}, function (_res) {
		var _data = "";
		_res.on('data', function (chunk) {
			_data += chunk;
		});
		_res.on('end', function () {
			res.send(_data);
		});
	}).on('error', function (e) {
		res.send(e);
	});
});
app.all("/feed/:vendor", function (req, res) {
	res.setHeader("content-type", "application/rss+xml");
	http.get({
		host: 'apiv2.spotboye.com',
		port: 80,
		path: '/feed/' + req.params.vendor
	}, function (_res) {
		var _data = "";
		_res.on('data', function (chunk) {
			_data += chunk;
		});
		_res.on('end', function () {
			res.send(_data);
		});
	}).on('error', function (e) {
		res.send(e);
	});
});

app.all("/hindi/feed/:id/:vendor", function (req, res) {
	res.setHeader("content-type", "application/rss+xml");
	http.get({
		host: 'apiv2.spotboye.com',
		port: 80,
		path: '/hi/feed/' + req.params.id + '/' + req.params.vendor
	}, function (_res) {
		var _data = "";
		_res.on('data', function (chunk) {
			_data += chunk;
		});
		_res.on('end', function () {
			res.send(_data);
		});
	}).on('error', function (e) {
		res.send(e);
	});
});
app.all("/hindi/feed/:vendor", function (req, res) {
	res.setHeader("content-type", "application/rss+xml");
	http.get({
		host: 'apiv2.spotboye.com',
		port: 80,
		path: '/hi/feed/' + req.params.vendor
	}, function (_res) {
		var _data = "";
		_res.on('data', function (chunk) {
			_data += chunk;
		});
		_res.on('end', function () {
			res.send(_data);
		});
	}).on('error', function (e) {
		res.send(e);
	});
});



// /* ROUTE
var handleAnything = function (req, res) {
	var file = (req.locale && req.locale == 'hi') ? 'index-hi.ejs' : 'index.ejs';
	if (req.device.type == "bot" || (typeof req.get('User-Agent') !== "undefined" && req.get('User-Agent').indexOf('PhantomJS') !== -1)) {
		res.render(file, {
			forceType: 'phone'
		});
	} else
		if (req.device.type == "tablet") {
			res.render(file, {
				forceType: 'phone'
			});
		} else {

			res.render(file);
		}

}

app.get("/.well-known/assetslinks.json", function (req, res) {
	var output = [{
		relation: ["delegate_permission/common.handle_all_urls"],
		target: {
			namespace: "android_app",
			package_name: "com.ninexe.ui",
			sha256_cert_fingerprints: ["DD:EC:4A:47:3D:52:2E:46:53:9D:C6:04:EA:C7:4D:2D:74:09:A6:04:78:9B:4A:5C:19:69:B4:59:0B:49:A8:D5"]
		}
	}];
	res.json(output);
});
app.get("/article/:id", function (req, res) {
	var articleId = req.params.id;
	var data = {
		forceType: 'desktop'
	}
	got('apiv2.spotboye.com/article/' + articleId, {
		json: true
	}).then(response => {
		data.article = response.body.data;
		res.render('amp-template/article-ucbrowser.ejs', data);
	}).catch(error => {
		res.status(502).send("Couldn't able to load")
	});


});
app.get("/hi/article/:id", function (req, res) {
	var articleId = req.params.id;
	var data = {
		forceType: 'desktop'
	}
	got('apiv2.spotboye.com/article/' + articleId, {
		json: true
	}).then(response => {
		data.article = response.body.data;
		res.render('amp-template/article-ucbrowser.ejs', data);
	}).catch(error => {
		res.status(502).send("Couldn't able to load")
	});


});


app.all('/category/*', function (req, res) {
	var url_param = "";
	if (req.params[0]) {
		url_param += "/" + req.params[0]
	}
	if (req.params[1]) {
		url_param += "/" + req.params[1]
	}
	res.redirect(301, "https://" + req.headers.host + url_param);
});
/** AMP RELATED ***/
var got = require('got');


String.prototype.replacerec = function (pattern, what) {
	var newstr = this.replace(pattern, what);
	if (newstr == this)
		return newstr;
	return newstr.replacerec(pattern, what);
};

function getClosingTags(htmlString) {
	return (htmlString.match(/<p/g) || []).length - (htmlString.match(/<\/p>/g) || []).length;
}

function moveTagToRoot(match, p1, offset, htmlString) {
	var closingTags = getClosingTags(htmlString.substring(0, offset));
	var replacement = p1;
	for (var i = 1; i <= closingTags; i++) {
		replacement = "</p>" + replacement + "<p>";
	}
	return replacement;
}

function blockquoteSrc(match, p1, offset, htmlString) {
	var replacement = p1;
	if (replacement.match(/twitter/g)) {
		replacement = replacement.replace(/<script([^>]*)src=(""|'')/g, '<script$1src="https://platform.twitter.com/widgets.js"');
	}
	return '<iframe>' + replacement + '</iframe>';
}
var article_amp = function (req, res) {
	var data = {
		forceType: 'desktop'
	};
	var menu = [];
	var current_url = 'https://' + req.get('host') + req.originalUrl.replace('/amp', '');

	var _fetch_menus = function () {
		got('apiv2.spotboye.com/appMenus', {
			json: true
		}).then(response => {
			console.log("Got menus API", response.body);
			cache.set('appmenus/raw', JSON.stringify(response.body.data));
			menu = response.body.data;
			_fetch_article();
		}).catch(error => {
			_fetch_article();
		});
	}
	//cache for menus
	if (cache) {
		cache.get('appmenus/raw', function (err, _menu) {
			if (err) {
				_fetch_menus();
			} else {
				console.log("Got cache menus", _menu);
				if (_menu) {
					menu = JSON.parse(_menu);
					_fetch_article();
				} else {
					_fetch_menus();
				}

			}
		});
	} else {
		_fetch_menus();
	}

	var _fetch_article = function () {
		var renderPage = function (_data) {
			data.article = _data;
			//console.log(data.article.body)
			data.menus = menu;
			//console.log(data.menus);
			var styleAttribute = /(<(?:img|p|span|a|em|iframe)\s+[^>]*?)style=".*?"/g,
				imgTag = /<img([^>]*?>)/g,
				ampOpenTag = /<(iframe|video)([^>]*?>)/g,
				ampCloseTag = /<\/(iframe|video)>/g,
				youtubeEmbed = /<iframe[^>]*?src="(?:(?:https?:)?\/\/)?(?:(?:www|m)\.)?(?:(?:youtube\.com|youtu.be))(?:\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(?:\S+)?"[^>]*?><\/iframe>/g,
				twitterEmbed = /<iframe>(<blockquote)((?:(?!blockquote).)*(?:https?:\/\/twitter\.com\/(?:#!\/)?(?:\w+)\/status(?:es)?\/(\d+))(?:(?!blockquote).)*<\/blockquote>)\s*(?:<script[^>]*?><\/script>)*<\/iframe>/g,
				instagramEmbed = /<iframe(?:(?!iframe).)*instagram\.com\/p\/([^\/]*)\/(?:(?!iframe).)*<\/iframe>/g,
				facebookEmbed = /<iframe[^>]*fbEmbed\?videoId=(\d+)[^>]*><\/iframe>/,
				script = /<script[^>]*>(?:(?!script).)*<\/script>/g;
			var modifiedhtmlString = getBody(data.article.body)
				.replace(youtubeEmbed, '<amp-youtube data-videoid="$1" layout="responsive" width="480" height="270"></amp-youtube>')
				.replace(twitterEmbed, '<amp-twitter width=486 height=657 layout="responsive" data-tweetid="$3">$1 placeholder$2</amp-twitter>')
				.replace(instagramEmbed, '<amp-instagram data-shortcode="$1" width="400" height="400" layout="responsive"></amp-instagram>')
				.replace(facebookEmbed, '<amp-facebook width=552 height=574 layout="responsive" data-embed-as="video" data-href="https://www.facebook.com/facebook/videos/$1/"></amp-facebook>')
				.replace(styleAttribute, '$1')
				.replace(imgTag, '<amp-img layout="responsive" $1<\/amp-img>')
				.replace(ampOpenTag, '<amp-$1$2')
				.replace(ampCloseTag, '<\/amp-$1>')
				.replace(script, '');

			data.article.body = modifiedhtmlString;
			data.current_url = current_url;
			res.render('amp-template/article.ejs', data);
		}
		var _req = function () {
			got('apiv2.spotboye.com/article/' + req.params.id, {
				json: true
			}).then(response => {

				if (response.body.data instanceof Array) {
					res.render('amp-template/not-found.ejs', data);
				} else {
					renderPage(response.body.data);
				}

			}).catch(error => {
				console.log('ERRRORR')
				res.render('amp-template/not-found.ejs', data);
			});
		}
		if (cache) {
			cache.get("article/" + req.params.id + "/raw", function (err, result) {
				if (err) {
					_req();
				} else {
					if (result) {
						renderPage(JSON.parse(result));
					} else {
						_req();
					}

				}
			});
		} else {
			_req();
		}
	}

}
var article_amp_hi = function (req, res) {
	var data = {
		forceType: 'desktop'
	};
	var menu = [];
	var current_url = 'https://' + req.get('host') + req.originalUrl.replace('/amp', '');

	var _fetch_menus = function () {
		got('apiv2.spotboye.com/appMenus', {
			json: true
		}).then(response => {
			console.log("Got menus API", response.body);
			cache.set('appmenus/raw', JSON.stringify(response.body.data));
			menu = response.body.data;
			_fetch_article();
		}).catch(error => {
			_fetch_article();
		});
	}
	//cache for menus
	if (cache) {
		cache.get('appmenus/raw', function (err, _menu) {
			if (err) {
				_fetch_menus();
			} else {
				console.log("Got cache menus", _menu);

				if (_menu) {
					menu = JSON.parse(_menu);
					_fetch_article();
				} else {
					_fetch_menus();
				}

			}
		});
	} else {
		_fetch_menus();
	}

	var _fetch_article = function () {
		var renderPage = function (_data) {
			data.article = _data;
			//console.log(data.article.body)
			data.menus = menu;
			//console.log(data.menus);
			var styleAttribute = /(<(?:img|p|span|a|em|iframe)\s+[^>]*?)style=".*?"/g,
				imgTag = /<img([^>]*?>)/g,
				ampOpenTag = /<(iframe|video)([^>]*?>)/g,
				ampCloseTag = /<\/(iframe|video)>/g,
				youtubeEmbed = /<iframe[^>]*?src="(?:(?:https?:)?\/\/)?(?:(?:www|m)\.)?(?:(?:youtube\.com|youtu.be))(?:\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(?:\S+)?"[^>]*?><\/iframe>/g,
				twitterEmbed = /<iframe>(<blockquote)((?:(?!blockquote).)*(?:https?:\/\/twitter\.com\/(?:#!\/)?(?:\w+)\/status(?:es)?\/(\d+))(?:(?!blockquote).)*<\/blockquote>)\s*(?:<script[^>]*?><\/script>)*<\/iframe>/g,
				instagramEmbed = /<iframe(?:(?!iframe).)*instagram\.com\/p\/([^\/]*)\/(?:(?!iframe).)*<\/iframe>/g,
				facebookEmbed = /<iframe[^>]*fbEmbed\?videoId=(\d+)[^>]*><\/iframe>/,
				script = /<script[^>]*>(?:(?!script).)*<\/script>/g;
			var modifiedhtmlString = getBody(data.article.body)
				.replace(youtubeEmbed, '<amp-youtube data-videoid="$1" layout="responsive" width="480" height="270"></amp-youtube>')
				.replace(twitterEmbed, '<amp-twitter width=486 height=657 layout="responsive" data-tweetid="$3">$1 placeholder$2</amp-twitter>')
				.replace(instagramEmbed, '<amp-instagram data-shortcode="$1" width="400" height="400" layout="responsive"></amp-instagram>')
				.replace(facebookEmbed, '<amp-facebook width=552 height=574 layout="responsive" data-embed-as="video" data-href="https://www.facebook.com/facebook/videos/$1/"></amp-facebook>')
				.replace(styleAttribute, '$1')
				.replace(imgTag, '<amp-img layout="responsive" $1<\/amp-img>')
				.replace(ampOpenTag, '<amp-$1$2')
				.replace(ampCloseTag, '<\/amp-$1>')
				.replace(script, '');

			data.article.body = modifiedhtmlString;
			data.current_url = current_url;
			res.render('amp-template/article-hi.ejs', data);
		}
		var _req = function () {
			got('apiv2.spotboye.com/article/' + req.params.id, {
				json: true
			}).then(response => {

				if (response.body.data instanceof Array) {
					res.render('amp-template/not-found.ejs', data);
				} else {
					renderPage(response.body.data);
				}

			}).catch(error => {
				console.log('ERRRORR')
				res.render('amp-template/not-found.ejs', data);
			});
		}
		if (cache) {
			cache.get("article/" + req.params.id + "/raw", function (err, result) {
				if (err) {
					_req();
				} else {
					if (result) {
						renderPage(JSON.parse(result));
					} else {
						_req();
					}

				}
			});
		} else {
			_req();
		}
	}

}

app.all('/amp/:cat/:subcat/:title/:id', article_amp);
app.all('/hi/amp/:cat/:subcat/:title/:id', article_amp_hi);
/** AMP RELATED ENDS ***/
function getFileContent(srcPath, callback) {
	fs.readFile(srcPath, 'utf8', function (err, data) {
		if (err) throw err;
		callback(data);
	});
}

//BOTS HANDLER
var handleBots = function (req, res) {
	var fetchArticle = function () {
		var articleId = req.params.id;
		var data = {}

		got('apiv2.spotboye.com/articlesbot/' + articleId, {
			json: true
		}).then(response => {
			cache.set('article/' + req.params.id + '/raw', JSON.stringify(response.body.data));
			data.article = response.body.data;
			data.article.ampURL = data.article.shareURL.replace('.com/', '.com/amp/');
			res.render('bots-template/article.ejs', data);
		}).catch(error => {
			res.status(404).send("Article not found");
		});
	}

	if (cache) {
		cache.get('article/' + req.params.id + '/raw', function (err, _article) {
			if (err) {
				console.log("Found err ", err);

				fetchArticle();
			}
			var data = {};
			if (_article) {
				console.log("Got article..");

				data.article = JSON.parse(_article);
				data.article.ampURL = data.article.shareURL.replace('.com/', '.com/amp/');
				res.render('bots-template/article.ejs', data);
			} else {
				fetchArticle();
			}

		});
	} else {
		fetchArticle();
	}



};
app.get("/:catid/:subcat/:title/:id", function (req, res) {
	//handleBots(req,res);
	if (req.device.type == "bot" || req.get('User-Agent').indexOf('WhatsApp') != -1 || req.get('User-Agent').indexOf('+https://developers.google.com/+/web/snippet/') != -1) {
		handleBots(req, res);
	} else {
		req.locale = 'en';
		handleAnything(req, res);
	}

});

app.get("/hi/:catid/:subcat/:title/:id", function (req, res) {
	//handleBots(req,res);
	if (req.device.type == "bot" || req.get('User-Agent').indexOf('WhatsApp') != -1 || req.get('User-Agent').indexOf('+https://developers.google.com/+/web/snippet/') != -1) {
		handleBots(req, res);
	} else {
		req.locale = 'hi';
		handleAnything(req, res);
	}

});
/** BOTS */
var handleCategoryBot = function (req, res) {
	var data = {};
	var url = '';
	if (req.params.subcat && req.params.subcat != '') {
		url = 'apiv2.spotboye.com/dashboardsubcatbot/' + req.params.subcat;
		data.isParent = false;
	} else {
		data.isParent = true;
		url = 'apiv2.spotboye.com/dashboardcatbot/' + req.params.catid;
	}
	if (req.device.type == "bot" || req.get('User-Agent').indexOf('WhatsApp') != -1 || req.get('User-Agent').indexOf('+https://developers.google.com/+/web/snippet/') != -1) {
		got(url, {
			json: true
		}).then(response => {

			// cache.set('/hi/dashboardarticle/' + req.params.id + '/raw', JSON.stringify(response.body.data));
			console.log("got cat response", response.body.data.length, response.body);

			data.category = response.body.data;
			res.render('bots-template/category.ejs', data);
		}).catch(error => {
			res.status(404).send(error);
		});
	} else {
		req.locale = 'en';
		handleAnything(req, res);
	}

}
var handleHindiCategoryBot = function (req, res) {
	var data = {};

	var url = '';
	if (req.params.subcat && req.params.subcat != '') {
		url = 'apiv2.spotboye.com/dashboardsubcatbot/' + req.params.subcat + "?lang=hi";
		data.isParent = false;
	} else {
		data.isParent = true;
		url = 'apiv2.spotboye.com/dashboardcatbot/' + req.params.catid + "?lang=hi";
	}
	if (req.device.type == "bot" || req.get('User-Agent').indexOf('WhatsApp') != -1 || req.get('User-Agent').indexOf('+https://developers.google.com/+/web/snippet/') != -1) {
		got(url, {
			json: true
		}).then(response => {
			console.log("got hindi cat response", response.body.data.length, response.body);

			data.category = response.body.data;
			res.render('bots-template/category.ejs', data);
		}).catch(error => {
			res.status(404).send("Page not found");
		});
	} else {
		req.locale = 'hi';
		handleAnything(req, res);
	}

}

var handleHindiDashboardBot = function (req, res) {
	var data = {};
	data.locale = 'hi/';
	if (req.device.type == "bot" || (typeof req.get('User-Agent') !== "undefined" && req.get('User-Agent').indexOf('PhantomJS') !== -1)) {

		got('apiv2.spotboye.com/dashboard?lang=hi', {
			json: true
		}).then(response => {
			// cache.set('/hi/dashboardarticle/' + req.params.id + '/raw', JSON.stringify(response.body.data));
			data.dashboard = response.body.data;
			res.render('bots-template/dashboard.ejs', data);
		}).catch(error => {
			res.status(404).send("Article not found");
		});

	} else {
		console.log("Its anything");
		req.locale = 'hi';
		handleAnything(req, res);
	}
}
app.get('/hi', handleHindiDashboardBot);
app.get('/hi/', handleHindiDashboardBot);
app.get("/hi/:catid", handleHindiCategoryBot);
app.get("/hi/:catid/:subcat", handleHindiCategoryBot);
app.get("/:catid", handleCategoryBot);
app.get("/:catid/:subcat", handleCategoryBot);

function copyFileContent(savPath, data) {
	fs.writeFile(savPath, data, function (err) {
		if (err) throw err;
		console.log('complete');
	});
}

app.all("/feed/:id/:vendor", function (req, res) {
	res.setHeader("content-type", "application/rss+xml");
	http.get({
		host: 'apiv2.spotboye.com',
		port: 80,
		path: '/feed/' + req.params.id + '/' + req.params.vendor
	}, function (_res) {
		var _data = "";
		_res.on('data', function (chunk) {
			_data += chunk;
		});
		_res.on('end', function () {
			res.send(_data);
		});
	}).on('error', function (e) {
		res.send(e);
	});
});
app.all("/feed/:vendor", function (req, res) {
	res.setHeader("content-type", "application/rss+xml");
	http.get({
		host: 'apiv2.spotboye.com',
		port: 80,
		path: '/feed/' + req.params.vendor
	}, function (_res) {
		var _data = "";
		_res.on('data', function (chunk) {
			_data += chunk;
		});
		_res.on('end', function () {
			res.send(_data);
		});
	}).on('error', function (e) {
		res.send(e);
	});
});

app.all("/healthcheck", function (req, res) {
	res.send("OK");
});

app.all('/*', handleAnything);
/*app.all('/profile', function (req, res) {
  res.render('index.ejs');
});

app.all('/login', function (req, res) {
  res.render('index.ejs');
});*/
app.all('/not-found', function (req, res) {
	res.status(404);
});

function shareLocaleURL(str, locale) {
	return str.replace('.com/', '.com/' + locale);
}

function getBody(htmlString) {
	var starsWithDiv = /^(<div>)/g;
	var modifiedhtmlString = starsWithDiv.test(htmlString) ? htmlString : '<div>' + htmlString + '</div>';
	var linebreaks = /\r?\n|\r/g,
		bold = /<b>((?:<br>)*)([^<]+?)((?:<br>)*)<\/b>/g,
		italic = /<i>((?:<br>)*)([^<]+?)((?:<br>)*)<\/i>/g,
		ignoreBIOpen = /<((b)|(i))((\s+[^>]*?)|)>/g,
		ignoreBIClose = /<\/((b)|(i))>/g,
		comments = /<!--[\s\S]*?-->/g,
		ignoredTagsOpen = /<((span)|(font)|(u)|(o:p))((\s+[^>]*?)|)>/g,
		ignoredTagsClose = /<\/((span)|(font)|(u)|(o:p))>/g,
		twttrScript = "<script>twttr.widgets.load();<\/script>",
		divOpen = /(<div)|(<h([1-6]))|(<span)/g,
		divClose = /(<\/div>)|(<\/h([1-6])>)|(<\/span>)/g,
		imgLink = /(<a[^>]*?>)(<img[^>]*?>)(<\/a>)/g,
		imgTag = /(<img[^>]*?>)/g,
		iframeTag = /(<iframe[\s\S^>]*?><\/iframe>)/g,
		instagramTag = /(<iframe(?:(?!iframe).)*instagram(?:(?!iframe).)*height="(\d+)")((?:(?!iframe).)*><\/iframe>)/g,
		pTag = /(<p[^>]*?>(?:(?!(?:<p|<\/p>)).)*<\/p>)/g,
		emptyImgTag = /<img([^>]*?)src=""([^>]*?)>/g,
		emptyPTag = /<p[^>]*?>(<br>)*(<script([^>]*?)><\/script>)*(\s)*<\/p>/g,
		blockquoteTag = /(<blockquote(?:(?!blockquote).)*<\/blockquote>\s*(?:<script([^>]*?)><\/script>)*)/g,
		multipleBr = /(\s*<br>\s*)+/g;
	return modifiedhtmlString
		.replace(linebreaks, " ")
		.replace(comments, "")
		.replace(ignoredTagsOpen, "")
		.replace(ignoredTagsClose, "")
		.replace(twttrScript, "")
		.replace(divOpen, "<p")
		.replace(divClose, "</p>")
		.replace(emptyImgTag, '')
		.replace(blockquoteTag, blockquoteSrc)
		.replace(imgLink, '$1link$3$2')
		.replace(imgTag, moveTagToRoot)
		.replace(iframeTag, moveTagToRoot)
		.replace(instagramTag, '$1 width="$2"$3')
		//.replacerec(pTag, moveTagToRoot)
		.replace(bold, '$1<strong>$2</strong>$3')
		.replace(italic, '$1<em>$2</em>$3')
		.replace(ignoreBIOpen, "")
		.replace(ignoreBIClose, "");
	//.replacerec(multipleBr, '</p><p>')
	//.replacerec(emptyPTag, '');
}

app.listen(post, function () {
	console.log('Listening on port 8080!')
});