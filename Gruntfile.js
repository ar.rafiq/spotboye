
/** LIVE RELOAD FOR GRUNT *****/
var LIVERELOAD_PORT = 35729;
var lrSnippet = require('connect-livereload')({ port: LIVERELOAD_PORT });
var livereloadMiddleware = function (connect, options) {
  return [
    lrSnippet,
    connect.static(options.base),
    connect.directory(options.base)
  ];
};

//Grunt is just JavaScript running in node, after all...
module.exports = function(grunt) {

  grunt.initConfig({
connect: {
  client: {
    options: {
      
      port: 9000,
      base:'client',
      middleware: livereloadMiddleware
    }
  }
},
watch: {
  client: {
    files: ['client/**/*'],
    tasks:[],
    options: {
      livereload:LIVERELOAD_PORT
    }
  }
},
	  
uglify: {
    my_target: {
		files:[{
      expand: true,
      src: ['assets/js/*.js','desktop/app.js','phone/app.js','providers/*.js'],
      dest: 'production',
      cwd: '.',
      rename: function (dst, src) {
        // To keep the source js files and make new files as `*.min.js`:
         return dst + '/' + src;
		  /*return dst + '/' + src.replace('.js', '.min.js');*/
        // Or to override to src:
       // return src;
      }
    }]
     
    }
  },
	  cssmin: {
  target: {
    files: [{
      expand: true,
      cwd: '.',
      src: ['assets/css/*.css', '!assets/css/*.min.css'],
      dest: 'production',
      ext: '.min.css'
    }]
  }
},
	   htmlmin: {                                     // Task
    dist: {                                      // Target
      options: {                                 // Target options
        removeComments: true,
        collapseWhitespace: true
      },
      files: [{
          expand: true,
          cwd: '.',
          src: ['desktop/**/*.html', 'phone/**/*.html',],
          dest: 'production'
      }]
    }
	   }
});

grunt.loadNpmTasks('grunt-contrib-uglify');
grunt.loadNpmTasks('grunt-contrib-cssmin');
grunt.loadNpmTasks('grunt-contrib-htmlmin');
	
grunt.registerTask('preview', ['connect:client','watch:client']);
grunt.registerTask('makeJS', ['uglify:my_target','cssmin:target','htmlmin']);
};