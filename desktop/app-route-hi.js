app.config(["$stateProvider", "$locationProvider", "$urlRouterProvider", "socialProvider", "$urlMatcherFactoryProvider", function ($stateProvider, $locationProvider, $urlRouterProvider, socialProvider, $urlMatcherFactoryProvider) {
	socialProvider.setFbKey({
		appId: "630344967097986",
		apiVersion: "v2.6"
	});


localStorage.setItem('locale', 'hi');

	$locationProvider.html5Mode(true);
	$urlMatcherFactoryProvider.strictMode(true);
	$urlRouterProvider.otherwise("/not-found");
	$stateProvider.state('home', {
		url: '/hi',
		templateUrl: '/' + DEVICE_TYPE + '/views/dashboard.html',
		controller: 'homeCtrl',
		params: {
			meta: {
				title: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywood Actress and Actors Updates | spotboye.com",
				description: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywood Actress and Actors Updates | spotboye.com"
			}
		}
	}).state('homes', {
		url: '/hi/',
		templateUrl: '/' + DEVICE_TYPE + '/views/dashboard.html',
		controller: 'homeCtrl',
		params: {
			meta: {
				title: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywood Actress and Actors Updates | spotboye.com",
				description: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywood Actress and Actors Updates | spotboye.com"
			}
		}
	})
		.state('celebrities', {
			url: '/hi/celebrity',
			templateUrl: '/' + DEVICE_TYPE + '/views/celebrities.html',
			controller: 'allCelebrityCtrl',
			params: {
				meta: {
					title: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywood Actress and Actors Updates | spotboye.com",
					description: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywood Actress and Actors Updates | spotboye.com"
				}
			}
		}).state('celebrities-search', {
			url: '/hi/celebrity/:keyword',
			templateUrl: '/' + DEVICE_TYPE + '/views/celebrities.html',
			controller: 'allCelebrityCtrl',
			params: {
				meta: {
					title: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywood Actress and Actors Updates | spotboye.com",
					description: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywood Actress and Actors Updates | spotboye.com"
				}
			}
		}).state('celebrity', {
			url: '/hi/celebrity/:celebrity/:id',
			templateUrl: '/' + DEVICE_TYPE + '/views/celebrity.html',
			controller: 'celebrityCtrl',
			params: {
				meta: {
					title: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywood Actress and Actors Updates | spotboye.com",
					description: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywood Actress and Actors Updates | spotboye.com"
				}
			}
		}).state('search', {
			url: '/hi/search?q',
			templateUrl: '/' + DEVICE_TYPE + '/views/search.html',
			controller: 'searchCtrl',
			params: {
				meta: {
					title: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywood Actress and Actors Updates | spotboye.com",
					description: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywood Actress and Actors Updates | spotboye.com"
				}
			}
		}).state('not-found', {
			url: '/hi/not-found',
			template: "<h3 class='container'>NOT FOUND</h3>"
		}).state('terms', {
			url: '/hi/terms',
			templateUrl: '/' + WEBSERVICE.DEVICE + '/views/terms.html'
		}).state('sitemap', {
			url: '/hi/sitemap',
			templateUrl: '/' + WEBSERVICE.DEVICE + '/views/sitemap.html'
		}).state('disclaimer', {
			url: '/hi/disclaimer',
			templateUrl: '/' + WEBSERVICE.DEVICE + '/views/disclaimer.html'
		}).state('privacypolicy', {
			url: '/hi/privacypolicy',
			templateUrl: '/' + WEBSERVICE.DEVICE + '/views/privacypolicy.html'
		}).state('contactus', {
			url: '/hi/contactus',
			templateUrl: '/' + WEBSERVICE.DEVICE + '/views/contactus.html'
		}).state('aboutus', {
			url: '/hi/aboutus',
			templateUrl: '/' + WEBSERVICE.DEVICE + '/views/about.html'
		}).state('logout', {
			url: '/logout',
			controller: function (Storage, $window) {
				Storage.clearUser();
				user_info = {
					islogged: false,
					userId: 0
				}
				$window.location.href = "/";
			}
		}).state('parentCategory', {
			url: '/hi/:category',
			templateUrl: '/' + DEVICE_TYPE + '/views/category.html',
			controller: 'categoryCtrl',
			params: {
				meta: {
					title: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywood Actress and Actors Updates | spotboye.com",
					description: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywood Actress and Actors Updates | spotboye.com"
				}
			}
		}).state('childCategory', {
			url: '/hi/:category/:subcategory',
			templateUrl: '/' + DEVICE_TYPE + '/views/category.html',
			controller: 'categoryCtrl',
			params: {
				meta: {
					title: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywood Actress and Actors Updates | spotboye.com",
					description: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywood Actress and Actors Updates | spotboye.com"
				}
			}
		}).state('article', {
			url: '/hi/:category/:subcategory/:title/:id',
			templateUrl: '/' + DEVICE_TYPE + '/views/article.html',
			controller: 'articleCtrl',
			params: {
				meta: {
					title: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywood Actress and Actors Updates | spotboye.com",
					description: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywood Actress and Actors Updates | spotboye.com",
				}
			}
		});
}]);